import torch.nn as nn
import torch
from tqdm import tqdm
import numpy as np

#                 criterion = nn.KLDivLoss()

def compute_fidelity(data, x_test):
    """
    Use the predicted values and compare them with the good test data with fidelity. Experimental version.
    """
    predictions = []

    for l in range(0, len(data)):
        predictions.append(np.sum(np.multiply(np.sqrt(data[l]),np.sqrt(x_test[l])))**2)
    return np.mean(predictions)


class NetworkModel(nn.Module):
    def __init__(self):
        super(NetworkModel, self).__init__()
        self.model = None
        self.loss_function = None

    def train_network(self, dataloader, epochs=10):

        optimizer = torch.optim.RMSprop(self.model.parameters())
        pbar = tqdm(total=epochs)
        for epoch in range(epochs):
            for i, (noisy, clean) in enumerate(dataloader):
                optimizer.zero_grad()

                estimated = self.model(noisy)

                loss = self.loss_function(estimated, clean)
                loss.backward()
                optimizer.step()
            pbar.update(1)

        pbar.close()

    def fidelity(self, dataloader):
        fidel_loss = []
        with torch.no_grad():
            for i, (noisy, clean) in enumerate(dataloader):
                estimated = self.model(noisy)

                fidel_loss.append(compute_fidelity(estimated.detach().cpu().numpy(), clean.detach().cpu().numpy()))
        return np.mean(fidel_loss)


class BaselineModel(NetworkModel):
    def __init__(self):
        super(NetworkModel, self).__init__()

    def train_network(self, dataloader, epochs=2):
        pass

    def fidelity(self, dataloader):
        fidel_loss = []
        with torch.no_grad():
            for i, (noisy, clean) in enumerate(dataloader):
                fidel_loss.append(compute_fidelity(noisy.detach().cpu().numpy(), clean.detach().cpu().numpy()))
        return np.mean(fidel_loss)


class NPJQINetwork(NetworkModel):
    def __init__(self, states_dimension, loss_function):
        super(NetworkModel, self).__init__()

        self.loss_function = loss_function

        self.model = nn.Sequential(
            nn.Linear(states_dimension, 25),
            nn.ReLU(),
            nn.Linear(25, 25),
            nn.ReLU(),
            nn.Linear(25, states_dimension),
            nn.Softmax(dim=1)
        )

    def forward(self, state):
        reconstructed_state = self.model(state)

        return reconstructed_state
