import argparse
import os
import numpy as np
import math
import sys

import torchvision.transforms as transforms
from torchvision.utils import save_image

from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import torch.nn as nn
import torch.nn.functional as F
import torch

os.makedirs("images", exist_ok=True)

parser = argparse.ArgumentParser()
parser.add_argument("--n_epochs", type=int, default=200, help="number of epochs of training")
parser.add_argument("--batch_size", type=int, default=64, help="size of the batches")
parser.add_argument("--lr", type=float, default=0.00005, help="learning rate")
parser.add_argument("--n_cpu", type=int, default=8, help="number of cpu threads to use during batch generation")
parser.add_argument("--latent_dim", type=int, default=100, help="dimensionality of the latent space")
parser.add_argument("--img_size", type=int, default=28, help="size of each image dimension")
parser.add_argument("--channels", type=int, default=1, help="number of image channels")
parser.add_argument("--n_critic", type=int, default=5, help="number of training steps for discriminator per iter")
parser.add_argument("--clip_value", type=float, default=0.01, help="lower and upper clip value for disc. weights")
parser.add_argument("--sample_interval", type=int, default=400, help="interval betwen image samples")



cuda = False if torch.cuda.is_available() else False


class Generator(nn.Module):
    def __init__(self, states_dimension):
        super(Generator, self).__init__()

        def block(in_feat, out_feat, normalize=True):
            layers = [nn.Linear(in_feat, out_feat)]

            layers.append(nn.LeakyReLU(0.2, inplace=True))
            return layers

        self.model = nn.Sequential(
            *block(states_dimension, 128, normalize=False),
            *block(128, 256),
            *block(256, 512),
            *block(512, states_dimension),
            #nn.Linear(states_dimension, 10),
            #nn.LeakyReLU(0.2, inplace=True),
            #nn.Linear(10, states_dimension),
            nn.Softmax()
        )

    def forward(self, z):
        img = self.model(z)
        #img = img.view(img.shape[0], *states_dimension)
        return img


class Discriminator(nn.Module):
    def __init__(self, states_dimension):
        super(Discriminator, self).__init__()

        self.model = nn.Sequential(
            nn.Linear(states_dimension, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(512, 256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Linear(256, 1),
        )

    def forward(self, img):
        #img_flat = img.view(img.shape[0], -1)
        validity = self.model(img)
        return validity






