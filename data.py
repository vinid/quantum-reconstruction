from torch.utils.data import Dataset
import numpy as np
import torch

cuda = True if torch.cuda.is_available() else False

Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor

class StatesDataset(Dataset):
    def __init__(self, X_noisy, X_clean):
        self.X_noisy = X_noisy
        self.X_clean = X_clean

        assert X_clean.shape == X_noisy.shape

    def __len__(self):
        return len(self.X_noisy)

    def __getitem__(self, idx):
        return Tensor(self.X_noisy[idx]), Tensor(self.X_clean[idx])


def load_dataset(fake_states, good_states):

    return StatesDataset(fake_states, good_states)
