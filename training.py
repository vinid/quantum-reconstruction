from data import load_dataset
import pandas as pd
from models import NPJQINetwork, BaselineModel
from torch import nn
import torch
from torch.utils.data import DataLoader

cuda = True if torch.cuda.is_available() else False

"""
Load Data
"""


fake_data = pd.read_csv("data/pure_noised.csv")
fake_data = fake_data[fake_data.columns[65:-3]].values

pure_data = pd.read_csv("data/pure_original.csv")
pure_data = pure_data[pure_data.columns[65:-3]].values

training_fake = fake_data[0:40000]
training_pure = pure_data[0:40000]

validation_fake = fake_data[40000:45000]
validation_pure = pure_data[40000:45000]

testing_fake = fake_data[45000:]
testing_pure = pure_data[45000:]

training_dataset = load_dataset(training_fake, training_pure)
validation_dataset = load_dataset(validation_fake, validation_pure)
testing_dataset = load_dataset(testing_fake, testing_pure)

training_dataset = DataLoader(training_dataset, batch_size=300, shuffle=True)
validation_dataset = DataLoader(validation_dataset, batch_size=300, shuffle=True)
testing_dataset = DataLoader(testing_dataset, batch_size=300, shuffle=True)

states_dimension = len(pure_data[0])

net = NPJQINetwork(states_dimension, nn.MSELoss())
base = BaselineModel()

if cuda:
    net.cuda()

net.train_network(training_dataset, epochs=500)

print(net.fidelity(training_dataset))
print(net.fidelity(testing_dataset))
print(base.fidelity(testing_dataset))
